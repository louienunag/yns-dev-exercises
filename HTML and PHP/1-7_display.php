<?php
    
    session_start(); 

    $first_name = $_SESSION['first_name'];
    $mid_name = $_SESSION['middle_name'];
    $last_name = $_SESSION['last_name'];
    $age = $_SESSION['age'];
    $email = $_SESSION['email'];
    
?>

<html>
<body>
    <h5>Full Name: <?= $first_name . ' ' . $mid_name . ' ' . $last_name ?></h5>
    <h5>Age: <?= $age ?> years old</h5>
    <h5>Email Address: <?= $email ?></h5>
</body>
</html>