<?php

    $number1 = 0;
    $number2 = 0;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $number1 = $_POST['txtnum1'];
        $number2 = $_POST['txtnum2'];

        $add = $number1 + $number2;
        $subtract = $number1 - $number2;
        $multiply = $number1 * $number2;
        $division = 0;

        if ($number2 != 0) {
            $division = $number1 / $number2;
        }

        echo nl2br("Addtion Operation Result: $add \n Substraction Operation Result: $subtract \n Multiplication Operation Result: $multiply \n Division Operation Result: $division \n\n");
    }

?>

<html>
<body>
    <form method="POST" action="">  
        <label>First Number:</label> 
        <input type="number" name="txtnum1" min="0" value="<?= $number1 ?>" />
        <label>Second Number:</label> 
        <input type="number" name="txtnum2" min="0" value="<?= $number2 ?>" />
        <input type="submit" value="Compute" />
    </form>
</body>
</html>