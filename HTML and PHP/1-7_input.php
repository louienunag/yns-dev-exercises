<?php
    
    session_start(); 

    $first_name = "";
    $mid_name = "";
    $last_name = "";
    $age = "";
    $email = "";

    if (isset($_POST['submit'])) {
        $error = 0;
        $first_name = $_POST['txtfirstname'];
        $mid_name = $_POST['txtmidname'];
        $last_name = $_POST['txtlastname'];
        $age = $_POST['txtage'];
        $email = $_POST['txtemailadd'];

        if (empty($first_name)) {
            $error = 1;
            echo nl2br("Please enter your first name. \n");
        }

        if (empty($mid_name)) {
            $error = 1;
            echo nl2br("Please enter your middle name. \n");
        }

        if (empty($last_name)) {
            $error = 1;
            echo nl2br("Please enter your last name. \n");
        }

        if (!is_numeric($age) && empty($age)) {
            $error = 1;
            echo nl2br("Please enter your age. \n");
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) && empty($email)) {
            $error = 1;
            echo nl2br("Please enter your valid email address. \n");
        }

        if ($error == 0) {
            $_SESSION['first_name'] = $first_name;
            $_SESSION['middle_name'] = $mid_name;
            $_SESSION['last_name'] = $last_name;
            $_SESSION['age'] = $age;
            $_SESSION['email'] = $email;

            header("location:1-7_display.php");
        } 

        echo nl2br("\n");
    } 

?>

<html>
<body>
    <form method="POST" action="">  
        <label>First Name</label>
        <input type="text" name="txtfirstname" value="<?= $first_name ?>" required>
        <br>
        <label>Middle Name</label>
        <input type="text" name="txtmidname" value="<?= $mid_name ?>" required>
        <br>
        <label>Last Name</label>
        <input type="text" name="txtlastname" value="<?= $last_name ?>" required>
        <br>
        <label>Age</label>
        <input type="number" name="txtage" min="1" value="<?= $age ?>" required>
        <br>
        <label>Email</label>
        <input type="email" name="txtemailadd" value="<?= $email ?>" required>
        <br>
        <input type="submit" name="submit" value="Submit" />
    </form>
</body>
</html>