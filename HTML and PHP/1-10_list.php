<html>
<body>
    <a href="1-10_input.php"><h6>Add User Information</h6></a>
    <table>
        <tr>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>
        <?php

            $csv_file = fopen("user_information.csv", "r");
            while (($line = fgetcsv($csv_file)) !== false) {
        
        ?>
        <tr>
            <?php foreach ($line as $content) { ?>   

                <td><?= htmlspecialchars($content) ?></td>

            <?php } ?>  
        </tr>
        <?php
        
            }
            fclose($csv_file);
        
        ?>
    </table>
</body>
</html>