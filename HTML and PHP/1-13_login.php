<?php

    session_start();

    if (isset($_SESSION['session_log'])) {
        header("location:1-13_list.php");
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $success = 0;
        $csv = "user_login.csv";
        $username = $_POST['txtusername'];
        $password = $_POST['txtpassword'];

        if (file_exists($csv)) {
            $csv_file = fopen($csv, "r");
            while (($content = fgetcsv($csv_file)) !== FALSE) {
                
                if ($content[6] == $username && $content[7] == $password) {
                    $success = 1;
                    break;
                }
            
            }
            fclose($csv_file);

            if ($success == 1) {
                $_SESSION['session_log'] =  $username;
                header("location:1-13_list.php");
            } 

            if ($success == 0) {
                echo nl2br("Wrong Username/Password! \n");
            }
        }
    }

?>

<html>
<body>
    <h4>Login Form</h4>
    <form method="POST" action="">
        <label>Username</label>
        <input type="text" name="txtusername" required>
        <br>
        <label>Password</label>
        <input type="password" name="txtpassword" required>
        <br>
        <input type="submit" value="Submit" />
    </form>
    <a href="1-13_input.php"><h5>Click here to sign up</h5></a>
</body>
</html>