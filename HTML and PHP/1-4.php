<?php
    
    $num = 0;
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num = $_POST['txtnum'];

        for ($i = 1; $i <= $num; $i++) {

            if ($i%3 == 0 && $i%5 == 0) {
                echo nl2br("FizzBuzz \n");
            } 

            if ($i%3 == 0 && $i%5 != 0) {
                echo nl2br("Fizz \n");
            }

            if ($i%5 == 0 && $i%3 != 0) {
                echo nl2br("Buzz \n");
            } 

            if ($i%3 != 0 && $i%5 != 0) {
                echo nl2br("$i \n");
            }

        }
        
        echo nl2br("\n");
    }

?>

<html>
<body>
    <form method="POST" action="">
        <label>Enter A Number:</label> 
        <input type="number" name="txtnum" min="0" value="<?= $num ?>" />
        <input type="submit" value="Submit" />
    </form>
</body>
</html>