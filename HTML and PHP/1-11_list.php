<?php
    
    $list = array();
    $csv_file = fopen("user_information_with_image.csv", "r");
    while (($line = fgetcsv($csv_file)) !== false) {
        
        if (!empty($line[1])) {
            array_push($list, $line);
        }

    }
    fclose($csv_file);

?>

<html>
<body>
    <a href="1-11_input.php"><h6>Add User Information</h6></a>
    <table>
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>
        <?php foreach ($list as $x => $content) { ?>
            
            <tr>
                <?php for ($i=0; $i <= 5; $i++) { ?>

                    <?php if ($i == 0) { ?>
                        <td><img src="<?= $content[$i] ?>" width='60' height='80'></td>
                    <?php } ?>
                    
                    <?php if ($i <= 5) { ?>
                        <td><?= htmlspecialchars($content[$i]) ?></td>
                    <?php } ?>

                <?php } ?>    
            </tr>

        <?php } ?>
    </table>
</body>
</html>