<?php
    
    session_start(); 

    $first_name = "";
    $mid_name = "";
    $last_name = "";
    $age = "";
    $email = "";
    $username = "";
    $password = "";

    if (isset($_POST['submit'])) {
        $error = 0;

        $full_file_path = $_SERVER['DOCUMENT_ROOT'] . "/yns-dev-exercises/images";
        if (!file_exists($full_file_path)) {
            mkdir($full_file_path, '0700');
        }

        $file_path = '../images';
        $file_name = $_FILES["txtimage"]["name"];
        $file = $file_path . '/' . date('YmdHis') . '-' . basename($file_name);
        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $file_size = $_FILES["txtimage"]["size"];

        $first_name = $_POST['txtfirstname'];
        $mid_name = $_POST['txtmidname'];
        $last_name = $_POST['txtlastname'];
        $age = $_POST['txtage'];
        $email = $_POST['txtemailadd'];

        if ($file_size > 5000000) {
            $error = 1;
            echo nl2br("Maximum file size of 5MB exceeded \n");
        }

        if ($file_type != "jpg" && $file_type != "png" && $file_type != "jpeg" && $file_type != "gif") {
            $error = 1;
            echo nl2br("Only .JPG, .JPEG, .PNG and .GIF file types are allowed. \n");
        }

        if ($error == 0) {
            $file_tmp_name = $_FILES["txtimage"]["tmp_name"];
            move_uploaded_file($file_tmp_name, $file);
            if (!file_exists($file)) {
                $error = 1;
                echo nl2br("Error in uploading image. \n");
            }
        }

        if (empty($first_name)) {
            $error = 1;
            echo nl2br("Please enter your first name. \n");
        }

        if (empty($mid_name)) {
            $error = 1;
            echo nl2br("Please enter your middle name. \n");
        }

        if (empty($last_name)) {
            $error = 1;
            echo nl2br("Please enter your last name. \n");
        }

        if (!is_numeric($age) && empty($age)) {
            $error = 1;
            echo nl2br("Please enter your age. \n");
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) && empty($email)) {
            $error = 1;
            echo nl2br("Please enter your valid email address. \n");
        }

        if ($error == 0) {
            $_SESSION['image'] = $file;
            $_SESSION['first_name'] = $first_name;
            $_SESSION['middle_name'] = $mid_name;
            $_SESSION['last_name'] = $last_name;
            $_SESSION['age'] = $age;
            $_SESSION['email'] = $email;

            header("location:1-13_display.php");
        } 

        echo nl2br("\n");
    }

?>

<html>
<body>
    <form method="POST" action="" enctype="multipart/form-data">  
        <label>Image</label>
        <input type="file" name="txtimage" required>
        <br>
        <label>First Name</label>
        <input type="text" name="txtfirstname" value="<?= $first_name ?>" required>
        <br>
        <label>Middle Name</label>
        <input type="text" name="txtmidname" value="<?= $mid_name ?>" required>
        <br>
        <label>Last Name</label>
        <input type="text" name="txtlastname" value="<?= $last_name ?>" required>
        <br>
        <label>Age</label>
        <input type="number" name="txtage" min="1" value="<?= $age ?>" required>
        <br>
        <label>Email</label>
        <input type="email" name="txtemailadd" value="<?= $email ?>" required>
        <br>
        <label>Username</label>
        <input type="text" name="txtusername" value="<?= $username ?>" required>
        <br>
        <label>Password</label>
        <input type="password" name="txtpassword" value="<?= $password ?>" required>
        <br>
        <input type="submit" name="submit" value="Submit" />
        <a href="1-13_list.php"><h6>Back to list</h6></a>
    </form>
</body>
</html>