<?php
    
    session_start(); 

    if (!isset($_SESSION['session_log'])) {
        header("location:1-13_login.php");
    }

    $list = array();
    $csv_file = fopen("user_login.csv", "r");
    while (($line = fgetcsv($csv_file)) !== false) {

        if (!empty($line[1])) {
            array_push($list, $line);
        }

    }
    $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
    $total = count($list);    
    $limit = 10;   
    $totalPages = ceil($total / $limit);
    $page = max($page, 1);
    $page = min($page, $totalPages);
    $offset = ($page - 1) * $limit;

    if($offset < 0) $offset = 0;

    $last_pageno = $page - 1;
    $next_pageno = $page + 1;
    $list = array_slice($list, $offset, $limit);
    fclose($csv_file);

?>

<html>
<body>
    <a href="1-13_input.php"><h6>Add User Information</h6></a>
    <a href="1-13_logout.php"><h6>Logout</h6></a>
    <table border="1" cellpadding="10">
    <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>
        <?php foreach ($list as $x => $content) { ?>
            
            <tr>
                <?php for ($i=0; $i <= 5; $i++) { ?>

                    <?php if ($i == 0) { ?>
                        <td><img src="<?= $content[$i] ?>" width='60' height='80'></td>
                    <?php } ?> 

                    <?php if ($i <= 5) { ?>
                        <td><?= htmlspecialchars($content[$i]) ?></td>
                    <?php } ?>

                <?php } ?>    
            </tr>

        <?php } ?>
    </table>
    <br>
    <div style="width: 300px;">
        <?php if($totalPages != 0) { ?>
            <?php if ($page > 1) { ?>
                <a href="1-13_list.php?page=<?= $last_pageno ?>" style="color: #c00">&#171; prev page</a>
            <?php } ?>
            
            <span>Page <strong><?= $page ?></strong> of <?= $totalPages ?></span>
            
            <?php if ($page != $totalPages) { ?>
                <a href="1-13_list.php?page=<?= $next_pageno ?>" style="color: #c00">next page &#187;</a>
            <?php } ?>
        <?php } ?>
    </div>
</body>
</html>