<?php
    
    session_start(); 

    $image = $_SESSION['image'];
    $first_name = $_SESSION['first_name'];
    $mid_name = $_SESSION['middle_name'];
    $last_name = $_SESSION['last_name'];
    $age = $_SESSION['age'];
    $email = $_SESSION['email'];
    
    $csv_file = fopen("user_information_with_image.csv", "a");
    $user_input = array($image, $first_name, $mid_name, $last_name, $age, $email);
    fputcsv($csv_file, $user_input);
    fclose($csv_file);
    
    session_destroy();
    
?>

<html>
<body>
    <h5>Image</h5>
    <img src='<?= $image ?>' width='100' height='100' />
    <h5>Full Name: <?= $first_name . ' ' . $mid_name . ' ' . $last_name ?></h5>
    <h5>Age: <?= $age ?> years old</h5>
    <h5>Email Address: <?= $email ?></h5>
    <a href="1-12_list.php"><h6>Back to list</h6></a>
</body>
</html>