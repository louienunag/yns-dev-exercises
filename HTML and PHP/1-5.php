<?php
    
    $date1 = date('m/d/Y');

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $date = $_POST['txtdate'];

        if (isset($date)) {
            $date1 = date('m/d/Y', strtotime($date));
            $date2 = date('m/d/Y', strtotime($date . ' +1 day'));;
            $date3 = date('m/d/Y', strtotime($date . ' +2 days'));

            $week1 = date('l', strtotime($date1));
            $week2 = date('l', strtotime($date2));
            $week3 = date('l', strtotime($date3));

            echo nl2br("Date #1: $date1 is $week1 \n");
            echo nl2br("Date #2: $date2 is $week2 \n");
            echo nl2br("Date #3: $date3 is $week3 \n\n");
        }
    }
?>

<html>
<body>
    <form method="POST" action="">
        <label>Enter Number:</label>
        <input type="date" name="txtdate" value="<?= date('m/d/Y', strtotime($date1)) ?>" />
        <input type="submit" value="Submit" />
    </form>
</body>
</html>