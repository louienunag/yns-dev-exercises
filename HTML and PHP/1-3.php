 <?php

    $number1 = 0;
    $number2 = 0;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $temp = 0;
        $number1 = $_POST['txtnum1'];
        $number2 = $_POST['txtnum2'];

        if ($number1 > $number2) {
            $temp = $number1;
            $number1 = $number2;
            $number1 = $temp;
        }

        for($i = 1; $i < ($number1+1); $i++) {
            
            if ($number1%$i == 0 && $number2%$i == 0)
                $gcd = $i;
            
        }

        echo nl2br("The GCD of $number1 and $number2 is: $gcd \n\n");
    }

?>

<html>
<body>
    <form method="POST" action="">
        <label>First Number:</label> 
        <input type="number" name="txtnum1" min="0" value="<?= $number1 ?>" />
        <label>Second Number:</label> 
        <input type="number" name="txtnum2" min="0" value="<?= $number2 ?>" />
        <input type="submit" value="Submit" />
    </form>
</body>
</html>