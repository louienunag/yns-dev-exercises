<?php

    if (isset($_GET['date'])) {
        $get_date = $_GET['date'];
    } else {
        $get_date = date('Y-m-01');
    }

    $prev = date('Y-m-01', strtotime('-1 month', strtotime($get_date)));
    $next = date('Y-m-01', strtotime('+1 month', strtotime($get_date)));
    $current_month = date('F Y', strtotime($get_date));
    $day_count = date('t', strtotime($get_date));
    $str = date('w', strtotime($get_date));

    $weeks = array();
    $week = '';
    $week .= str_repeat('<td></td>', $str);

    $today = date('Y-m-j');

    for ($day = 1; $day <= $day_count; $day++) {
        $date = date('Y-m', strtotime($get_date)) . '-' . $day;
         
        if ($today == $date) {
            $week .= '<td class="today">' . $day;
        } else {
            $week .= '<td>' . $day;
        }

        $week .= '</td>';
         
        if ($str % 7 == 6 || $day == $day_count) {
            if ($day == $day_count) {
                $week .= str_repeat('<td></td>', 6 - ($str % 7));
            }

            $weeks[] = '<tr>' . $week . '</tr>';

            $week = '';
        }

        $str++;
    }

?>

<html>

<head>
        
    <style>

        th {
            height: 30px;
            text-align: center;
            border: 1px solid black;
        }

        td {
            height: 50px;
            border: 1px solid black;
        }

        a {
            text-decoration: none;
        }

        .today {
            background: #FFFF99;
        }

        .header {
            border: none;
        }
    
    </style>

</head>

<body>

    <table style="width: 50%;">

        <tr>
            <th class="header" colspan="7">

                <button title="Previous Month" onclick="window.location.href='?date=<?= $prev ?>'">&laquo;</button>

                &nbsp;
                
                <span><?= $current_month ?></span>
                
                &nbsp;
                
                <button title="Next Month" onclick="window.location.href='?date=<?= $next ?>'">&raquo;</button>
                
            </th>    
        </tr>

        <tr>
            <th>Sunday</th>
            <th>Monday</th>
            <th>Tuesday</th>
            <th>Wednesday</th>
            <th>Thursday</th>
            <th>Friday</th>
            <th>Saturday</th>
        </tr>
        
        <?php
            
            foreach ($weeks as $week) {
                echo $week;
            }
        
        ?>
    
    </table>

</body>

</html>