<?php

    $database = mysqli_connect("localhost", "root", "", "yns_exercise") or die ("Database Error!");

    $score = 0;
    $percentage = 0;
    $message = '';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        foreach ($_POST as $question_id => $answer_id) {
            $score_sql = "SELECT correct_answer FROM quiz_answers WHERE question_id = " . $question_id . " AND id = " . $answer_id . " LIMIT 1";
            $score_query = mysqli_query($database, $score_sql);

            while($score_row = mysqli_fetch_array($score_query)) {
                $score += $score_row[0];
            }
        }
    }
    
    $percentage = ($score / 10) * 100;

    if ($score >= 5) {
        $message = 'Congratulations, You Passed the Quiz!';
    } else {
        $message = 'Sorry, You Failed the Quiz.';
    }
    
?>


<html>

<body>

    <h3>Quiz Results</h3>

    <h4><?= $message ?></h4>

    <h4>Your Score: <?= round($percentage) . '% (' . $score . ' points)' ?></h4>
    <h4>Passing Score: <?= '50% (5 points)' ?></h4>

    <br>

    <a href="6-1_input.php">Retake Quiz</a>

</body>

</html>