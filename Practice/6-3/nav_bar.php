
  <div class="navbar">
    <div class="dropdown">
      <button class="dropbtn">PHP Exercises &#9660;</button>
      <div class="dropdown-content">
        <a href="../PHP/1-1.php">1-1 Show Hello World.</a>
        <a href="../PHP/1-2.php">1-2 The four basic operations of arithmetic.</a>
        <a href="../PHP/1-3.php">1-3 Show the greatest common divisor.</a>
        <a href="../PHP/1-4.php">1-4 Solve FizzBuzz problem.</a>
        <a href="../PHP/1-5.php">1-5 Input date. Then show 3 days from inputted date and its day of the week.</a>
        <a href="../PHP/1-6_input.php">1-6 Input user information. Then show it in next page.</a>
        <a href="../PHP/1-7_input.php">1-7 Add validation in the user information form(required, numeric, character, mailaddress).</a>
        <a href="../PHP/1-8_input.php">1-8 Store inputted user information into a CSV file.</a>
        <a href="../PHP/1-9_list.php">1-9 Show the user information using table tags.</a>
        <a href="../PHP/1-10_list.php">1-10 Upload images.</a>
        <a href="../PHP/1-11_list.php">1-11 Show uploaded images in the table.</a>
        <a href="../PHP/1-12_list.php">1-12 Add pagination in the list page.</a>
        <a href="../PHP/1-13_login.php">1-13 Create login form and embed it into the system that you developed</a>
      </div>
    </div> 
    
    <div class="dropdown">
      <button class="dropbtn">JS Exercise &#9660;</button>
      <div class="dropdown-content">
        <a href="../JS/2-1.php">2-1 Show alert.</a>
        <a href="../JS/2-2.php">2-2 Confirm dialog and redirection</a>
        <a href="../JS/2-3.php">2-3 The four basic operations of arithmetic</a>
        <a href="../JS/2-4.php">2-4 Show prime numbers.</a>
        <a href="../JS/2-5.php">2-5 Input characters in text box and show it in label.</a>
        <a href="../JS/2-6.php">2-6 Press button and add a label below button.</a>
        <a href="../JS/2-7.php">2-7 Show alert when you click an image.</a>
        <a href="../JS/2-8.php">2-8 Show alert when you click link.</a>
        <a href="../JS/2-9.php">2-9 Change text and background color when you press buttons.</a>
        <a href="../JS/2-10.php">2-10 Scroll screen when you press buttons.</a>
        <a href="../JS/2-11.php">2-11 Change background color using animation.</a>
        <a href="../JS/2-12.php">2-12 Show another image when you mouse over an image. Then show the original image when you mouse out.</a>
        <a href="../JS/2-13.php">2-13 Change size of images when you press buttons.</a>
        <a href="../JS/2-14.php">2-14 Show images according to the options in combo box.</a>
        <a href="../JS/2-15.php">2-15 Show current date and time in real time.</a>
      </div>
    </div>

    <div class="dropdown">
      <button class="dropbtn">Pratice Exercise &#9660;</button>
      <div class="dropdown-content">
        <a href="../Docker/6-1_input.php">Quiz with three multiple choices</a>
        <a href="../Docker/6-2.php">Calendar</a>
        <a href="../index.php">Navigation</a>
      </div>
    </div>
  </div>

  <br>