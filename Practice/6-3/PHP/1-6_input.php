<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="1-6_display.php">

        <label>First Name</label>
        <input type="text" name="txtfirstname">

        <br>

        <label>Middle Name</label>
        <input type="text" name="txtmidname">

        <br>

        <label>Last Name</label>
        <input type="text" name="txtlastname">

        <br>

        <label>Age</label>
        <input type="number" name="txtage" min="1">

        <br>

        <label>Email</label>
        <input type="email" name="txtemailadd">

        <br>
        
        <input type="submit" value="Submit" />

    </form>

</body>
</html>