<?php
    
    $date1 = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['txtdate'])) {
            $date1 = date('Y-m-d', strtotime($_POST['txtdate']));
            $date2 = date('Y-m-d', strtotime($_POST['txtdate'] . ' +1 day'));;
            $date3 = date('Y-m-d', strtotime($_POST['txtdate'] . ' +2 days'));

            $week1 = date('l', strtotime($date1));
            $week2 = date('l', strtotime($date2));
            $week3 = date('l', strtotime($date3));

            echo 'Date #1: ' . $date1 . ' is ' . $week1 . '<br>';
            echo 'Date #2: ' . $date2 . ' is ' . $week2 . '<br>';
            echo 'Date #3: ' . $date3 . ' is ' . $week3 . '<br>';
        }
    }
?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="">

        <label>Enter Number:</label>
        <input type="date" name="txtdate" value="<?= date('m/d/Y', strtotime($date1)) ?>" />

        <input type="submit" value="Submit" />

    </form>

</body>

</html>