<?php
    
    session_start(); 

    if (!isset($_SESSION['session_log'])) {
        header("location:1-13_login.php");
    }

    $list = array();

    $csv_file = fopen("user_login.csv", "r");

    while (($line = fgetcsv($csv_file)) !== false) {
        if (!empty($line[1])) {
            array_push($list, $line);
        }
    }

    $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
    $total = count($list); //total items in array    
    $limit = 10; //per page    
    $totalPages = ceil($total / $limit); //calculate total pages
    $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
    $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
    $offset = ($page - 1) * $limit;
    if($offset < 0) $offset = 0;

    $list = array_slice($list, $offset, $limit);

    fclose($csv_file);

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <a href="1-13_input.php"><h6>Add User Information</h6></a>
    <a href="1-13_logout.php"><h6>Logout</h6></a>

    <table border="1" cellpadding="10">
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>

        <?php

            foreach ($list as $x => $content) {
                echo "<tr>";

                for ($i=0; $i <= 5; $i++) { 
                    if ($i == 0) {
                        echo "<td><img src='" . $content[$i] . "' width='60' height='80'></td>";
                    } elseif ($i <= 5) {
                        echo "<td>" . htmlspecialchars($content[$i]) . "</td>";
                    }
                }    

                echo "</tr>";
            }

        ?>

    </table>

    <br>
    
    <?php

        $link = '1-13_list.php?page=%d';
        $pagerContainer = '<div style="width: 300px;">';   
        if( $totalPages != 0 ) 
        {
          if( $page == 1 ) 
          { 
            $pagerContainer .= ''; 
          } 
          else 
          { 
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> &#171; prev page</a>', $page - 1 ); 
          }
          $pagerContainer .= ' <span> page <strong>' . $page . '</strong> from ' . $totalPages . '</span>'; 
          if( $page == $totalPages ) 
          { 
            $pagerContainer .= ''; 
          }
          else 
          { 
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> next page &#187; </a>', $page + 1 ); 
          }           
        }                   
        $pagerContainer .= '</div>';

        echo $pagerContainer;

    ?>    

</body>

</html>