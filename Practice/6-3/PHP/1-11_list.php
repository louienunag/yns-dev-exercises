<?php
    
    $list = array();

    $csv_file = fopen("user_information_with_image.csv", "r");

    while (($line = fgetcsv($csv_file)) !== false) {
        if (!empty($line[1])) {
            array_push($list, $line);
        }
    }

    fclose($csv_file);

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <a href="1-11_input.php"><h6>Add User Information</h6></a>

    <table>
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>

        <?php

            foreach ($list as $x => $content) {
                echo "<tr>";

                for ($i=0; $i <= 5; $i++) { 
                    if ($i == 0) {
                        echo "<td><img src='" . $content[$i] . "' width='60' height='80'></td>";
                    } elseif ($i <= 5) {
                        echo "<td>" . htmlspecialchars($content[$i]) . "</td>";
                    }
                }    

                echo "</tr>";
            }

        ?>

    </table>

</body>

</html>