<?php

    $x = 0;
    $y = 0;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $operator = $_POST['txtoperator'];
        $x = $_POST['txtnum1'];
        $y = $_POST['txtnum2'];

        if ($operator == 'add') {
            $total = $x + $y;
            echo "Addtion Operation Result: $total";
        } elseif ($operator == "subtract") {
            $total = $x - $y;
            echo "Substraction Operation Result: $total";
        } elseif ($operator == "multiply") {
            $total = $x * $y;
            echo "Multiplication Operation Result: $total";
        } elseif ($operator == "divide") {
            if ($y == 0) {
                echo "You cannot divide a number by zero!";
            } else {
                $total = $x / $y;
                echo "Division Operation Result: $total";
            }
        }
    }

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
    <form method="POST" action="">
        
        <label>First Number:</label> 
        <input type="number" name="txtnum1" min="0" value="<?= $x ?>" />

        <label>Second Number:</label> 
        <input type="number" name="txtnum2" min="0" value="<?= $y ?>" />

        <input type="submit" name="txtoperator" value="add" />
        <input type="submit" name="txtoperator" value="subtract" />
        <input type="submit" name="txtoperator" value="multiply" />
        <input type="submit" name="txtoperator" value="divide" />

    </form>

</body>

</html>