<?php
    
    session_start(); 

    $first_name = $_SESSION['first_name'];
    $mid_name = $_SESSION['middle_name'];
    $last_name = $_SESSION['last_name'];
    $age = $_SESSION['age'];
    $email = $_SESSION['email'];
    
    $csv_file = fopen("user_information.csv", "a");

    $user_input = array($first_name, $mid_name, $last_name, $age, $email);

    fputcsv($csv_file, $user_input);

    fclose($csv_file);
    
    session_destroy();
    
?>


<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <h5>Full Name: <?= $first_name . ' ' . $mid_name . ' ' . $last_name ?></h5>
    <h5>Age: <?= $age ?> years old</h5>
    <h5>Email Address: <?= $email ?></h5>

    <a href="1-10_list.php"><h6>Back to list</h6></a>

</body>

</html>