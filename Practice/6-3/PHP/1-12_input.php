<?php
    
    session_start(); 

    $first_name = "";
    $mid_name = "";
    $last_name = "";
    $age = "";
    $email = "";

    if (isset($_POST['submit'])) {
        $error = 0;

        $full_file_path = $_SERVER['DOCUMENT_ROOT'] . "/yns-dev-exercises/images";
        if (!file_exists($full_file_path)) {
            mkdir($full_file_path, '0700');
        }

        $file_path = '../images';
        $file = $file_path . '/' . date('YmdHis') . '-' . basename($_FILES["txtimage"]["name"]);
        $file_type = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        $first_name = $_POST['txtfirstname'];
        $mid_name = $_POST['txtmidname'];
        $last_name = $_POST['txtlastname'];
        $age = $_POST['txtage'];
        $email = $_POST['txtemailadd'];

        if ($_FILES["txtimage"]["size"] > 5000000) {
            $error = 1;
            echo "Maximum file size of 5MB exceeded";
        }

        if ($file_type != "jpg" && $file_type != "png" && $file_type != "jpeg" && $file_type != "gif") {
            $error = 1;
            echo "Only .JPG, .JPEG, .PNG and .GIF file types are allowed.";
        }

        if ($error == 0) {
            move_uploaded_file($_FILES["txtimage"]["tmp_name"], $file);
            if (!file_exists($file)) {
                $error = 1;
                echo "Error in uploading image.";
            }else{
                $_SESSION['image'] = $file;
            }
        }

        if (empty($_POST['txtfirstname'])) {
            $error = 1;
            echo "Please enter your first name. <br>";
        } else {
            $_SESSION['first_name'] = $_POST['txtfirstname'];
        }

        if (empty($_POST['txtmidname'])) {
            $error = 1;
            echo "Please enter your middle name. <br>";
        } else {
            $_SESSION['middle_name'] = $_POST['txtmidname'];
        }

        if (empty($_POST['txtlastname'])) {
            $error = 1;
            echo "Please enter your last name. <br>";
        } else {
            $_SESSION['last_name'] = $_POST['txtlastname'];
        }

        if (!is_numeric($_POST['txtage']) && empty($_POST['txtage'])) {
            $error = 1;
            echo "Please enter your age. <br>";
        } else {
            $_SESSION['age'] = $_POST['txtage'];
        }

        if (!filter_var($_POST['txtemailadd'], FILTER_VALIDATE_EMAIL) && empty($_POST['txtemailadd'])) {
            $error = 1;
            echo "Please enter your valid email address. <br>";
        } else {
            $_SESSION['email'] = $_POST['txtemailadd'];
        }

        if ($error == 0) {
            header("location:1-12_display.php");
        } 
    }

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="" enctype="multipart/form-data">
        
        <label>Image</label>
        <input type="file" name="txtimage" required>

        <br>

        <label>First Name</label>
        <input type="text" name="txtfirstname" value="<?= $first_name ?>" required>

        <br>

        <label>Middle Name</label>
        <input type="text" name="txtmidname" value="<?= $mid_name ?>" required>

        <br>

        <label>Last Name</label>
        <input type="text" name="txtlastname" value="<?= $last_name ?>" required>

        <br>

        <label>Age</label>
        <input type="number" name="txtage" min="1" value="<?= $age ?>" required>

        <br>

        <label>Email</label>
        <input type="email" name="txtemailadd" value="<?= $email ?>" required>

        <br>

        <input type="submit" name="submit" value="Submit" />
        <a href="1-12_list.php"><h6>Back to list</h6></a>

    </form>

</body>

</html>