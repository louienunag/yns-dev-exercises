 <?php

    $x = 0;
    $y = 0;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $temp = 0;
        $x = $_POST['txtnum1'];
        $y = $_POST['txtnum2'];

        if ($x > $y) {
            $temp = $x;
            $x = $y;
            $y = $temp;
        }

        for($i = 1; $i < ($x+1); $i++) {
            if ($x%$i == 0 && $y%$i == 0)
                $gcd = $i;
        }

        echo "The GCD of $x and $y is: $gcd";
    }

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="">

        <label>First Number:</label> 
        <input type="number" name="txtnum1" min="0" value="<?= $x ?>" />

        <label>Second Number:</label> 
        <input type="number" name="txtnum2" min="0" value="<?= $y ?>" />

        <input type="submit" value="Submit" />

    </form>

</body>

</html>