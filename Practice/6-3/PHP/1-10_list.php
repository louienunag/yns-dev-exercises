<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <a href="1-10_input.php"><h6>Add User Information</h6></a>

    <table>
        <tr>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>

        <?php
        
            $csv_file = fopen("user_information.csv", "r");
            while (($line = fgetcsv($csv_file)) !== false) {
                echo "<tr>";
                foreach ($line as $content) {
                    echo "<td>" . htmlspecialchars($content) . "</td>";
                }
                echo "</tr>";
            }
            fclose($csv_file);

        ?>

    </table>

</body>

</html>