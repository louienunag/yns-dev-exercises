<?php
    
    session_start(); 

    if (!isset($_SESSION['session_log'])) {
        $current_session = $_SESSION['session_log'];
    } else {
        $current_session = $_SESSION['session_log'];
    }

    $current_session = $_SESSION['session_log'];

    $image = $_SESSION['image'];
    $first_name = $_SESSION['first_name'];
    $mid_name = $_SESSION['middle_name'];
    $last_name = $_SESSION['last_name'];
    $age = $_SESSION['age'];
    $email = $_SESSION['email'];
    $username = $_SESSION['username'];
    $password = $_SESSION['password'];
    
    $csv_file = fopen("user_login.csv", "a");

    $user_input = array($image, $first_name, $mid_name, $last_name, $age, $email, $username, $password);

    fputcsv($csv_file, $user_input);

    fclose($csv_file);
    
    session_unset(); //remove all user inputs

    $_SESSION['session_log'] =  $current_session;
    
?>


<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <h3>User Added Successfully!</h3>

    <h5>Image</h5>
    <img src='<?= $image ?>' width='100' height='100' />
    <h5>Full Name: <?= $first_name . ' ' . $mid_name . ' ' . $last_name ?></h5>
    <h5>Age: <?= $age ?> years old</h5>
    <h5>Email Address: <?= $email ?></h5>

    <a href="1-13_list.php"><h6>Back to list</h6></a>

</body>

</html>