<?php
    
    $num = 0;
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $num = $_POST['txtnum'];

        for ($i = 1; $i <= $num; $i++) {
            if ($i%3 == 0 && $i%5 == 0) {
                echo 'FizzBuzz <br>';
            } elseif ($i%3 == 0) {
                echo 'Fizz <br>';
            } elseif ($i%5 == 0) {
                echo 'Buzz <br>';
            } else {
                echo "$i <br>";
            }
        }
    }

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="">

        <label>Enter A Number:</label> 
        <input type="number" name="txtnum" min="0" value="<?= $num ?>" />

        <input type="submit" value="Submit" />

    </form>

</body>

</html>