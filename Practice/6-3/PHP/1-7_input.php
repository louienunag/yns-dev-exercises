<?php
    
    session_start(); 

    $first_name = "";
    $mid_name = "";
    $last_name = "";
    $age = "";
    $email = "";

    if (isset($_POST['submit'])) {
        $error = 0;
        $first_name = $_POST['txtfirstname'];
        $mid_name = $_POST['txtmidname'];
        $last_name = $_POST['txtlastname'];
        $age = $_POST['txtage'];
        $email = $_POST['txtemailadd'];

        if (empty($_POST['txtfirstname'])) {
            $error = 1;
            echo "Please enter your first name. <br>";
        } else {
            $_SESSION['first_name'] = $_POST['txtfirstname'];
        }

        if (empty($_POST['txtmidname'])) {
            $error = 1;
            echo "Please enter your middle name. <br>";
        } else {
            $_SESSION['middle_name'] = $_POST['txtmidname'];
        }

        if (empty($_POST['txtlastname'])) {
            $error = 1;
            echo "Please enter your last name. <br>";
        } else {
            $_SESSION['last_name'] = $_POST['txtlastname'];
        }

        if (!is_numeric($_POST['txtage']) && empty($_POST['txtage'])) {
            $error = 1;
            echo "Please enter your age. <br>";
        } else {
            $_SESSION['age'] = $_POST['txtage'];
        }

        if (!filter_var($_POST['txtemailadd'], FILTER_VALIDATE_EMAIL) && empty($_POST['txtemailadd'])) {
            $error = 1;
            echo "Please enter your valid email address. <br>";
        } else {
            $_SESSION['email'] = $_POST['txtemailadd'];
        }

        if ($error == 0) {
            header("location:1-7_display.php");
        }
    } 

?>

<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

    <form method="POST" action="">
        
        <label>First Name</label>
        <input type="text" name="txtfirstname" value="<?= $first_name ?>" required>

        <br>

        <label>Middle Name</label>
        <input type="text" name="txtmidname" value="<?= $mid_name ?>" required>

        <br>

        <label>Last Name</label>
        <input type="text" name="txtlastname" value="<?= $last_name ?>" required>

        <br>

        <label>Age</label>
        <input type="number" name="txtage" min="1" value="<?= $age ?>" required>

        <br>

        <label>Email</label>
        <input type="email" name="txtemailadd" value="<?= $email ?>" required>

        <br>

        <input type="submit" name="submit" value="Submit" />

    </form>

</body>

</html>