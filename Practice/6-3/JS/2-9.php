<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
	<button id="button1" onclick="showColors('yellow-violet');">Yellow font and Violet background</button>

	<br><br>

	<button id="button2" onclick="showColors('blue-orange');">Blue font and Orange background</button>

	<br><br>

	<button id="button3" onclick="showColors('red-green');">Red font and Green background</button>

<script type="text/javascript">

	function showColors(color) {

		if (color == 'yellow-violet') {
			document.getElementById('button1').style.color = 'yellow';
			document.getElementById('button1').style.backgroundColor = 'violet';
		}else if (color == 'blue-orange') {
			document.getElementById('button2').style.color = 'blue';
			document.getElementById('button2').style.backgroundColor = 'orange';
		}else if (color == 'red-green') {
			document.getElementById('button3').style.color = 'red';
			document.getElementById('button3').style.backgroundColor = 'green';
		}

	}

</script>

</body>

</html>