<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
    <label>Text</label>
    <input type="text" id="txtbox" onkeyup="updateText();" />

    <br>

    Result: <label id="lbltext"></label>

<script type="text/javascript">

	function updateText() {
		var text = document.getElementById("txtbox").value;
		var label = document.getElementById("lbltext");

		label.innerHTML = text;
	}

</script>

</body>

</html>