<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
	<div id="effect" style="width: 500px; height: 500px; background-color: #00008b;"></div>
 
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">

	var myVar = setInterval(changeBackground, 2000);
	var state = true;

	function changeBackground() {
			
		if (state) {
			$("#effect").animate({
				backgroundColor: "#add8e6",
				width: 500
			}, 1000);
		} else {
			$("#effect").animate({
				backgroundColor: "#00008b",
				width: 500
			}, 1000);
		}

		state = !state;

	}

</script>

</body>

</html>