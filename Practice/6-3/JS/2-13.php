<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>

	<img id="image" src="../images/pic1.jpg" style="width: 50px; height: 50px;">

	<br><br>

	<button onclick="changeSize('small');">Small</button>

	&nbsp;&nbsp;

	<button onclick="changeSize('medium');">Medium</button>

	&nbsp;&nbsp;

	<button onclick="changeSize('large');">Large</button>

<script type="text/javascript">

	function changeSize(size) {
		
		if (size == 'small') {
			document.getElementById('image').style.height = '50px';
			document.getElementById('image').style.width = '50px';
		} else if (size == 'medium') {
			document.getElementById('image').style.height = '100px';
			document.getElementById('image').style.width = '100px';
		} else if (size == 'large') {
			document.getElementById('image').style.height = '200px';
			document.getElementById('image').style.width = '200px';
		}

	}

</script>

</body>

</html>