<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
    <label>Max Number</label>
    <input type="number" id="txtnum1" min="2" />

    <br>

    <button onclick="showPrime();">Submit</button>

<script>
	
	function showPrime() {
		var num1 = parseInt(document.getElementById("txtnum1").value);
		var primes = "";

		for (i = 2; i <= num1; i++) {
		    var flag = 0;

		    for (j = 2; j < i; j++) {
		        if (i % j == 0) {
		            flag = 1;
		            break;
		        }
		    }

		    if (i > 1 && flag == 0) {
		    	primes += i;
		    	primes += ', ';
		    }
		}

		alert('The Prime Numbers are ' + primes);
	}

</script>

</body>

</html>