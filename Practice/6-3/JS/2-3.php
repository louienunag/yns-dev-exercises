<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
	<label>First Number</label>
    <input type="number" id="txtnum1" min="0" />

    <br>

    <label>Second Number</label>
    <input type="number" id="txtnum2" min="0" />

    <br>

    <label>Operation Type</label>
    <select id="txtoperator">
        <option value="1">Addition</option>
        <option value="2">Substraction</option>
        <option value="3">Multiplication</option>
        <option value="4">Division</option>
    </select>

    <br>

    <button onclick="showAnswer();">Compute</button>

<script>

	function showAnswer() {
		var num1 = parseInt(document.getElementById("txtnum1").value);
		var num2 = parseInt(document.getElementById("txtnum2").value);
		var type = document.getElementById("txtoperator").value;

		if (type == 1) {
			var result = num1 + num2;
			alert('The answer is ' + result);
		} else if (type == 2) {
			var result = num1 - num2;
			alert('The answer is ' + result);
		} else if (type == 3) {
			var result = num1 * num2;
			alert('The answer is ' + result);
		} else if (type == 4) {
			if (num2 == 0) {
				alert('You cannot divide by zero!');
			}else{
				var result = num1 / num2; 
				alert('The answer is ' + result);
			}
		}
	}

</script>

</body>

</html>