<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
    <button onclick="createLabel();">Add New Label</button>

    <div id="divlabels"></div>

<script type="text/javascript">

	var ctr = 1;

	function createLabel() {

	    var divlabels = document.getElementById('divlabels');

	    var label = document.createElement('label');
	    label.innerHTML = "This is Label #" + ctr;

	    var br = document.createElement('br');

	    ctr++;

	    divlabels.appendChild(label);
	    divlabels.appendChild(br);

	}

</script>

</body>

</html>