<html>

<head>

  <link rel="stylesheet" type="text/css" href="../nav_style.css">

</head>

<body>

    <?php

        include_once '../nav_bar.php';

    ?>
    
<button id="button1" onclick="goBack(this);">Click Me!</button>

<script>
	function goBack(id) {
		var goto = confirm("Back to Exercise 1?");
        if (goto == true) {
            window.location.href = "2-1.html";
        }
	}
</script>

</body>

</html>