<?php

    $database = mysqli_connect("localhost", "root", "", "yns_exercise") or die ("Database Error!");

    $question_no = 1;
    $questions = array();

    $question_sql = "SELECT * FROM quiz_problems ORDER BY RAND()";
    $question_query = mysqli_query($database, $question_sql);

    while($question_row = mysqli_fetch_array($question_query, MYSQLI_ASSOC)) {
        $questions[] = $question_row;
    }

?>

<html>

<body>

    <form method="POST" action="6-1_display.php">

    <?php 

        foreach ($questions as $question) { 

            $answers = array();

            $answer_sql = "SELECT id, choices FROM quiz_answers WHERE question_id = " . $question['id'] . " ORDER BY RAND()";
            $answer_query = mysqli_query($database, $answer_sql);

            while($answer_row = mysqli_fetch_array($answer_query, MYSQLI_ASSOC)) {
                $answers[] = $answer_row;
            }

    ?>

        <hr>

        <label><?= 'Question #' . $question_no++ . ': ' . $question['question']  ?></label>

        <br>

        <?php 

            foreach ($answers as $i => $answer) {

        ?>

            <label for="<?= $answer['id'] ?>">
                <input type="radio" id="<?= $answer['id'] ?>" name="<?= $question['id'] ?>" <?= ($i==0) ? 'checked' : '' ?> value="<?= $answer['id'] ?>">
                <?= $answer['choices'] ?>
            </label>

            &nbsp;&nbsp;

        <?php

            }

        ?>

        <br><br>

    <?php 
        
        } 

    ?>

    <button type="submit">Submit</button>

    </form>

</body>

</html>