<?php
    
    session_start(); 

    $database = mysqli_connect("localhost", "root", "", "yns_exercise") or die ("Database Error!");

    if (!isset($_SESSION['session_log'])) {
        header("location:3-5_login.php");
    }

    $list = array();

    $sql = "SELECT image, first_name, middle_name, last_name, age, email FROM user_information ORDER BY user_id ASC";
    $query = mysqli_query($database, $sql);
    while($row = mysqli_fetch_array($query)) {
        $list[] = $row;
    }

    $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
    $total = count($list); //total items in array    
    $limit = 10; //per page    
    $totalPages = ceil($total / $limit); //calculate total pages
    $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
    $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
    $offset = ($page - 1) * $limit;
    if($offset < 0) $offset = 0;

    $list = array_slice($list, $offset, $limit);

?>

<html>

<body>

    <a href="3-5_input.php"><h6>Add User Information</h6></a>
    <a href="3-5_logout.php"><h6>Logout</h6></a>

    <table border="1" cellpadding="10">
        <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Email Address</th>
        </tr>

        <?php

            foreach ($list as $x => $content) {
                echo "<tr>";

                for ($i=0; $i <= 5; $i++) { 
                    if ($i == 0) {
                        echo "<td><img src='" . $content[$i] . "' width='60' height='80'></td>";
                    } elseif ($i <= 5) {
                        echo "<td>" . htmlspecialchars($content[$i]) . "</td>";
                    }
                }    

                echo "</tr>";
            }

        ?>

    </table>

    <br>
    
    <?php

        $link = '3-5_list.php?page=%d';
        $pagerContainer = '<div style="width: 300px;">';   
        if( $totalPages != 0 ) 
        {
          if( $page == 1 ) 
          { 
            $pagerContainer .= ''; 
          } 
          else 
          { 
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> &#171; prev page</a>', $page - 1 ); 
          }
          $pagerContainer .= ' <span> page <strong>' . $page . '</strong> from ' . $totalPages . '</span>'; 
          if( $page == $totalPages ) 
          { 
            $pagerContainer .= ''; 
          }
          else 
          { 
            $pagerContainer .= sprintf( '<a href="' . $link . '" style="color: #c00"> next page &#187; </a>', $page + 1 ); 
          }           
        }                   
        $pagerContainer .= '</div>';

        echo $pagerContainer;

    ?>    

</body>

</html>